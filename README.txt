
This module adds a input format filter that replaces wrong Romanian dicritic
marks with their correct entries.

See http://kitblog.com/2008/10/romanian_diacritic_marks.html for a good
explanation.

INSTALLATION AND CONFIGURATION
==============================
- Install the module as any other Drupal module.
- Go to admin/settings/filters, select the desired filter and activate the
  "Correct Romanian diacritic marks" filter.

AUTHOR AND MAINTAINER
=====================

Claudiu Cristea (claudiu.cristea)
http://drupal.org/user/56348
